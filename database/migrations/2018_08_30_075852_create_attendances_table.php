<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendancesTable extends Migration
{

    public function up()
    {
        Schema::create('attendances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->date('date');
            $table->time('begin');
            $table->time('end');
            $table->softDeletes();
            $table->timestamps();
        });
    }



    public function down()
    {
        Schema::dropIfExists('attendances');
    }
}
