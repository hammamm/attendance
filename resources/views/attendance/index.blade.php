@extends('layouts.master')
@section('title',"Attendance")
@section('content')
<div class="contaier" id="date">

    <label for="day">{{__('attendanceIndex.day')}}</label>
    <input type="date" v-model='date' v-on:keyup.enter="searchInput()"/>
    <br><br>
    <table class="table">
        <thead>
            <th>{{__('attendanceIndex.employee')}}</th>
            <th>{{__('attendanceIndex.begin')}}</th>
            <th>{{__('attendanceIndex.end')}}</th>
        </thead>
        <tbody v-for="(user,id) in users">
            <td>@{{user.username}}</td>
            <td><input type="time" name="begin" v-model="user.begin" v-on:keyup.enter="updateBegins(id)"/></td>
            <td><input type="time" name="end" v-model="user.end" v-on:keyup.enter="updateEnds(id)" /></td>
        </tbody>
    </table>
</div>
@endsection('content')
@section('script')
@include('scripts.attendanceIndex')
@endsection('script')
