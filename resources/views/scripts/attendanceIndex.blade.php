<script>
var date = new Vue({
  el:'#date',
  data:{
    date: '{{ date('Y-m-d') }}',
    users:[],
    token:'{{csrf_token()}}'
  },
  created: function() {

          this.$http.get('/allusers').then(result => {
            this.users = [];
            for (var i = 0; i < result.body.users.length; i++) {
              this.users.push({
                'id':result.body.users[i].id,
                'username':result.body.users[i].name,
                'begin':result.body.users[i].begin,
                'end':result.body.users[i].end,
              });
          }
          });
      },
  methods:{
    searchInput: function(){
      this.$http.post('/userwhere',{date:this.date,_token: this.token}).then(result => {
        console.log(this.token);
        console.log(this.date);
        this.users = [];
        for (var i = 0; i < result.body.users.length; i++) {
          this.users.push({
            'id':result.body.users[i].id,
            'username':result.body.users[i].name,
            'begin':result.body.users[i].begin,
            'end':result.body.users[i].end,
          });
      }
      });
    },
    updateEnds:function(id){
      this.$http.post('/updateends',{
        date:this.date,
        '_token': this.token,
        user:this.users[id].id,
        end:this.users[id].end
      }).then(result => {
        console.log(this.token);
        console.log(this.date);
        console.log(result);


      });
    },
    updateBegins:function(id){
      this.$http.post('/updateBegins',{
        date:this.date,
        '_token': this.token,
        user:this.users[id].id,
        begin:this.users[id].begin
      }).then(result => {
        console.log(this.token);
        console.log(this.date);
        console.log(result);


      });
    }
  }

});
</script>
