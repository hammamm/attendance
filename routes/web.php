<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/w', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'AttendanceController@index')->name('home');
Route::get('/allusers', 'AttendanceController@allusers');
Route::post('/userwhere', 'AttendanceController@userWhere');
Route::post('/updateends', 'AttendanceController@updateEnds');
Route::post('/updateBegins', 'AttendanceController@updateBegins');
