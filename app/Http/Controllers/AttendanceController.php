<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\attendance;
use Carbon\Carbon;
class AttendanceController extends Controller
{
    public function index(){
      return view('attendance.index');
    }
    public function allusers(){
      return response()->json(['users'=>
        attendance::select('attendances.id','begin','end','users.name')->join('users', 'users.id', '=', 'attendances.user_id')->where('date',date('Y-m-d'))->get()
      ]);
    }
    public function userWhere(Request $request){
      return response()->json(['users'=>
        attendance::select('attendances.id','begin','end','users.name')->join('users', 'users.id', '=', 'attendances.user_id')->where('date',$request->input('date'))->get()
      ]);
    }
    public function updateEnds(Request $request){
      if(attendance::where(['date'=>$request->input('date'),'user_id'=>$request->input('user')])->update(['end'=>$request->input('end')]))
        return response()->json(['status'=>true]);
    }
    public function updateBegins(Request $request){
      if(attendance::where(['date'=>$request->input('date'),'user_id'=>$request->input('user')])->update(['begin'=>$request->input('begin')]))
        return response()->json(['status'=>true]);
    }
}
