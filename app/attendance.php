<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class attendance extends Model
{
  use SoftDeletes;
  protected $fillable = ['user_id','begin','end','date'];
  function User(){
    return $this->belongTo(User::class, 'id', 'user_id');
  }


}
